# Lab Assignment solution
Files containing the solution for the Lab Assignment (practical part) for AE3001FE, year 2023/2024.

## Files description
1. `Lab Assignment_solution.ipynb`: 
    The assignment file with questions and solutions.
2. `test_solutions.py` and `tests_lab2.py`: 
    python module to check correctness pf answers in the jupyter notebook.
3.  `mesh.py`: required module with mesh handling functions.

## Installation instructions
You need to have a working python environment. We strongly recomment using `conda`, which allows you to use several different python versions with different packages installed at the same time in an organized way.

To be able to debug your code we also recommend using `visual studio code`.

Nevertheless, if you feel lazy and do not need debugging, you can still use google colab.

The following guidelines can be followed for
 1. Installation on own machine (windows, linux, and macOS)
 2. Installation on google colab (you need a google account, google drive, and google colab)

### Own machine

#### 0. Windows and macOS
1. Install miniforge

    Go the link below and download the `Miniforge3` version that suits your system (e.g., on Windows the `Miniforge3-Windows-x86_64`, on macOS choose the x86_64 version if you have an older mac with an Intel processor `Miniforge3-MacOSX-x86_64`, if you have a newer mac with the new M1, or M2 processors then select the arm64 version `Miniforge3-Windows-arm64`)
   
    https://github.com/conda-forge/miniforge

    Follow the instructions.
    
    **IMPORTANT**: On Windows, in the install window, select the option to add conda miniforge to your PATH, this will make your life much easier.

2. Install visual studio code
    
    Google for visual studio code, most likely the first option is the one you need. Download `visual studio code  ` and install it.
	
3.  Install Python extension to visual studio code

    https://marketplace.visualstudio.com/items?itemName=ms-python.python

#### 0. Linux
If you are using linux, you know what you are doing. So, you have your own IDE for python. Just make sure you install conda, so that we can create environments. If you feel adventurous, proceed without environments.

#### 1. All environments: Setup your conda environment to run the code
1. Create conda environment (on your terminal, console, etc)
    ```bash
    conda create --name AP3001_lab python=3.10 
    ```
2. Activate the python environment
    ```bash
    conda activate AP3001_lab
	```
3. Install required python dependencies
	```bash
	conda install numpy scipy matplotlib ipython notebook=6.4.12 jupyter_contrib_nbextensions
	pip3 install scikit-fem[all]
	```  

#### 2. All environments: Get the code for the assignment
The code is available in the following git repository:

https://gitlab.tudelft.nl/ap3001fe/2023_2024/lab-assignment

We recommend you download it, instead of cloning it (since you will not be commiting anything to the repo).

Once you download it, unzip the file into a directory of your choice, let us assume the path to this folder is `your_assignment_folder`.

#### 3. All environments: Open your code
Open `visual studio code` and open the folder `your_assignment_folder` containing the files with the code for the assignment.

#### 4. All environments: Associate your conda python to visual studio code
You need to associate your conda python to `visual studio code`. Once you do this, you will be able to (i) run the `check_python_is_ok.ipynb` file and (ii) open and run the `Lab Assignment.ipynb` file containing the assignment.

If you run `check_python_is_ok.ipynb` you should get the correct output. If that is the case, your environment is properly setup.

### Installation on Google colab
To use Google colab you need
1. Google account
2. Google drive setup
3. [Google colab](https://colab.research.google.com/) activated

#### Get the code for the assignment
The code is available in the following git repository:

https://gitlab.tudelft.nl/ap3001fe/2023_2024/lab-assignment

We recommend you download it, instead of cloning it (since you will not be commiting anything to the repo).

Once you download it, unzip the file into a directory of your choice, let us assume the path to this folder is `your_assignment_folder`.


#### Upload the code to google drive
In google drive, create a folder in the root of google drive. You can click [here](https://drive.google.com/drive/u/0/my-drive) to go to your google drive root.

Right click on the blank screen below the list of folders and files in your google drive and select `New folder`. In the window asking for the name of the folder, type `AP3001_FE`. You should now see the folder in your google drive. Open the folder by double clicking it.

Again, create a folder, right click with your mouse and select `New folder`. In the window asking for the name of the folder, type `Lab_assignment`. You should now see the folder in your google drive. Open it by double clicking.

Now upload all the contents of your local folder `your_assignment_folder` to the google drive folder `Lab_assignment`. You just need to select all files (and folders) and drag them into your google drive (it should have the `Lab_assignment` folder open).

You should now see all files inside the google drive.

#### Open `check_python_is_ok.ipynb`
Now we just need to open the `check_python_is_ok.ipynb` file to make sure everything is working fine. To do that, you will need to click on the three vertical dots on the right of the line with the file `check_python_is_ok.ipynb`. Then on the dropdown menu, click on `Open with`, then there are two options: either you see `Google Colaboratory` as an option or not. 

If you see it, you can just click it and go to the next step. 

If not, then you need to first click on `Connect more apps`. You should now see `Google Workspace Marketplace`. In the `Search apps` search, type `Colaboratory`. You should now see it as the first option below on the left: a large orange C and O. Once you click on it, you will be asked if you wish to intall it, say yes, and follow the instructions. You can now go back the start of this section, and when you click the `Open with` you should now see `Google Colaboratory` as an option. Once you click it, you should open the notebook.


#### Run `check_python_is_ok.ipynb`
You can now run the notebook. If the output is as expected, then all is fine and you are good to go.


## Working on your assignment
To work on your assignment, you just need to open it following the same instructions are you did for the `check_python_is_ok.ipynb`. You can now edit your assignment to answer your questions. Do not forget to keep saving the notebook with your answers. In the end, download it, and submit it using Brightspace.























### 0. Python environment
You need to have a working python environment. We strongly recomment using `conda`, which allows you to use several different python versions with different packages installed at the same time in an organized way.

To be able to debug your code we also recommend using `visual studio code`.

The following guidelines can be followed:

#### Windows and macOS
1. Install miniforge

    Go the link below and download the `Miniforge3` version that suits your system (e.g., on Windows the `Miniforge3-Windows-x86_64`, on macOS choose the x86_64 version if you have an older mac with an Intel processor `Miniforge3-MacOSX-x86_64`, if you have a newer mac with the new M1, or M2 processors then select the arm64 version `Miniforge3-Windows-arm64`)
   
    https://github.com/conda-forge/miniforge

    Follow the instructions.

2. Install visual studio code
    
    Google for visual studio code, most likely the first option is the one you need. Download `visual studio code  ` and install it.
	
3.  Install Python extension to visual studio code

    https://marketplace.visualstudio.com/items?itemName=ms-python.python

#### Linux
If you are using linux, you know what you are doing. So, you have your own IDE for python. Just make sure you install conda, so that we can create environments. If you feel adventurous, proceed without environments.

### 1. Setup your conda environment to run the code
1. Create conda environment
    ```bash
    conda create --name AP3001_lab python=3.10 
    ```
2. Activate the python environment
    ```bash
    conda activate AP3001_lab
	```
3. Install required python dependencies
	```bash
	conda install numpy scipy matplotlib ipython notebook=6.4.12 jupyter_contrib_nbextensions
	pip3 install scikit-fem[all]
	```  

### 2. Get the code for the assignment
The code is available in the following git repository:

https://gitlab.tudelft.nl/ap3001fe/2023_2024/lab-assignment-solution

We recommend you download it, instead of cloning it (since you will not be commiting anything to the repo).

Once you download it, unzip the file into a directory of your choice.

### 3. Open the code
Open `visual studio code` and open the folder containing the files with the code for the assignment.

### 4. Associate your conda python to visual studio code
You need to associate you conda python to `visual studio code`. Once you do this, you will be able to (i) run the `check_python_is_ok.py` file and (ii) open and run the `Lab Assignment_solution.ipynb` file containing the assignment.
